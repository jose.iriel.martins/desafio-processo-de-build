export class PointList {
    constructor() {
        this.list = [];

        this.selectElements();
        this.registerEvents();
    }

    selectElements() {
        this.form = document.querySelector("#point-form");
        this.imageInput = document.querySelector("#input-image");
        this.imageInputLabel = document.querySelector("#input-image-label");
        this.titleInput = document.querySelector("#input-title");
        this.descriptionInput = document.querySelector("#input-description");

        this.imagePreview = document.querySelector("#image-preview");
        this.points = document.querySelector("#point-wrapper");
    }

    registerEvents() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
        this.imageInput.addEventListener("change", this.renderImagePreview.bind(this));
    }

    addItemToList(event) {
        event.preventDefault();

        const imageFile = event.target["input-image"].files[0];
        const title = event.target["input-title"].value;
        const description = event.target["input-description"].value;

        if (imageFile && title.length && description.length) {
            loadImage(imageFile, (e) => {
                this.list.push({
                    image: e.target.result,
                    title,
                    description,
                });

                this.renderListItems();
            });

            this.resetInputs();
        }
    }

    renderImagePreview(event) {
        const imageFile = event.target.files[0];

        if (imageFile) {
            loadImage(imageFile, (e) => {
                this.imagePreview.setAttribute("src", e.target.result);
                this.imagePreview.style.opacity = 1;
                this.imageInputLabel.style.opacity = 0;
            });
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach((item) => {
            itemsStructure += `
                <div class="point">
                    <img class="point-image" src="${item.image}" alt="Imagem do ponto turístico." />
                    <div class="point-text">
                        <h2 class="point-title">${item.title}</h2>
                        <p class="point-description">${item.description}</p>
                    </div>
                </div>`;

            this.points.innerHTML = itemsStructure;
        });
    }

    resetInputs() {
        this.imageInput.value = "";
        this.imagePreview.style.opacity = 0;
        this.imageInputLabel.style.opacity = 1;
        this.titleInput.value = "";
        this.descriptionInput.value = "";
    }
}

function loadImage(file, func) {
    const fileReader = new FileReader();
    fileReader.addEventListener("load", func);
    fileReader.readAsDataURL(file);
}
